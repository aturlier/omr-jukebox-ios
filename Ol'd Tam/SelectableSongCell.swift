//
//  SelectableSongCell.swift
//  Ol'd Tam
//
//  Created by Axel Turlier on 06/06/2017.
//  Copyright © 2017 LP IEM. All rights reserved.
//

import UIKit

class SelectableSongCell : UITableViewCell {
    let song : Song? = nil
    
    @IBOutlet weak var artImageView: UIImageView!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var lyricsButton: UIButton!
    @IBOutlet weak var albumNameLabel: UILabel!

    func initWithSong (song : Song) {
        // INIT VIEW
    }
    
    func changeSelectedValue() {
        //Change
        //Network calls
    }
}
